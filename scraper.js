(function() {
    var fs = require('fs');
    var request = require('request');
    var cheerio = require('cheerio');

    var express = require('express');
    var app = express();

    var links = require("./links");

    app.get('/scrape', function (req, res) {

        console.log("Tumepata request, sasa ngoja tusafiri mpaka nse");

        links.get_links(function (error, data) {
            if (error === null) {
                console.log("Data " + JSON.stringify(data));
                for (var count = 0; count < data.length; count++) {
                    scraper.scrap_site(data[count], req, res);
                }
            }else{
                console.log("Error getting links")
            }
        });

    }).listen('8081');

    console.log('Listening on port 8081');

    var scraper = function () {

    };

    scraper.scrap_site = function (url, req, res) {
        var self = this;

        request(url, function (error, response, html) {
            if (!error) {
                var $ = cheerio.load(html);

                var resp_data = [];

                var table_stats = $('.marketStats tbody tr');

                table_stats.each(function(index, elem){
                    var this_row = $(this);
                    var data = this_row.find('td');

                    var info = {};

                    if(this_row.attr('class') == 'row0' || this_row.attr('class') == 'row1') {
                        var company   = data.eq(0).text();
                        var prev_price= data.eq(1).text();
                        var curr_price= data.eq(2).text();

                        var change    = data.find('.uchange').text();

                        var info = {
                                "company": company,
                                "Prev price": prev_price,
                                "Curr price": curr_price,
                                "Change": change
                            }

                    }else{
                        var td = this_row.find('td');
                        if(td.length < 2){
                            /**
                             * Probably this is where the details are
                             */
                            var date = data.find('strong').eq(0).text();

                            if(date != "" )
                               info = { "date" : date};
                        }
                    }

                    if( Object.keys(info).length !== 0 )
                        resp_data.push(info);
                });

                res.write(JSON.stringify(resp_data));

                self.done(req, res);
            }
        });
    };

    scraper.done = function(req, res){
        res.statusCode = 200;
        res.send();
    }

})();